import * as functions from 'firebase-functions';
import * as admin       from 'firebase-admin';

import express      from 'express';
import bodyParser   from "body-parser";

const app = express();
const main = express();

main.use('/go', app);
main.use(bodyParser.json());
main.use(bodyParser.urlencoded({ extended: true }));


export const goto = functions.https.onRequest(main);

app.get('/:shortcode', async (req, res) => {
    const short = req.params.shortcode;

    admin.firestore().collection('urls').doc(short).get()
    .then(val => {
        const data = val.data();
        if (val.exists && data) {
            incrementCount(val);
            res.redirect(data.longurl);
        }
        res.send(`No URL for ${short}`);
    })
    .catch(e => {
        console.error(e)
        res.send(`Couldn't find code ${short}`);
    })
    
});


function incrementCount(doc: admin.firestore.DocumentSnapshot): void {
    const increment = admin.firestore.FieldValue.increment(1);
    doc.ref.update({ clickCount: increment }).catch(e => console.error(e))
}